# atmospheric-deposition-shiny

## R shiny application to facilitate entry, upload, and quality control for CAP LTER atmospheric deposition (wet-dry bucket, bulk) data. 

#### overview

This is the codebase for the Shiny application that facilitates data entry and quality control for the CAP LTER's long-term monitoring of atmospheric deposition. This project has a long history focusing on different sites and lotic systems throughout the greater Phoenix metropolitan area and surrounding Sonoran desert, but focuses now only on a single bulk and wet-dry collector on the roof of the Life Sciences A building of Arizona State University, Tempe, Arizona, USA.

