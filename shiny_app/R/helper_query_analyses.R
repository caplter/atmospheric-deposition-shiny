#' @title atmospheric_deposition analysis details from the database
#'
#' @description Makes available the list of analyses ids and names ordered by
#' frequency of occurrence for convenience.

query_analyses <- function() {

  base_query <- "
  SELECT
    analysis.id,
    analysis.name,
    analysis.instrument
  FROM atmospheric_deposition.analysis
  -- JOIN (
  --     SELECT
  --       DISTINCT(analysis_id)
  --     FROM atmospheric_deposition.results
  --     ) AS inuse ON (inuse.analysis_id = analysis.id)
  -- JOIN (
  --     SELECT
  --       analysis_id,
  --       COUNT(analysis_id)
  --     FROM atmospheric_deposition.results
  --     GROUP BY analysis_id
  --     ) AS frequency ON (frequency.analysis_id = analysis.id)
  -- ORDER BY frequency.count DESC
  ;
  "

  common_analyses <- run_interpolated_query(base_query)

  return(common_analyses)

}

analyses <- query_analyses()

analyses_names <- analyses |>
  dplyr::pull(name)
