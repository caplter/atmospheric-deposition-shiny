# libraries --------------------------------------------------------------------

library(shiny)
library(dplyr)
library(tidyr)
library(readr)
library(DT)
library(stringr)
library(lubridate)
library(DBI)
library(RPostgreSQL)
library(pool)
library(readxl)
library(glue)
library(R6)


# options ----------------------------------------------------------------------

options(shiny.maxRequestSize = 30 * 1024^2) # increase max file upload size
options(shiny.reactlog = FALSE)


# configuration ----------------------------------------------------------------

# configuration from config.yml
this_configuration <- config::get(config = "default")

# database connection
this_pool <- pool::dbPool(
  drv      = RPostgreSQL::PostgreSQL(),
  dbname   = this_configuration$dbname,
  host     = this_configuration$host,
  user     = this_configuration$user,
  password = this_configuration$password
)

shiny::onStop(function() {
  pool::poolClose(this_pool)
})


# supporting modules, functions, and configurations ----------------------------

source("R/helper_sql_execution.R") # ensure that this is loaded first


# R6 ---------------------------------------------------------------------------

# generate objects
# ChemViewer1 <- ChemViewer$new(id = "chem_display")
